<?php
/*
Plugin Name:  Mailhog Setup
Description:  Set up Mailhog on this site
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/

function mailhog_setup_init( $phpmailer ) {
	$phpmailer->isSMTP();
	$phpmailer->Host = 'mail';
	$phpmailer->Port = 1025;
}
add_action( 'phpmailer_init', 'mailhog_setup_init' );
