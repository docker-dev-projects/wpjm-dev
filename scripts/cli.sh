#!/bin/bash

set -e

NETWORK=wpjm-dev-net
WP_CONTAINER=wpjm-dev_wordpress_1

if [ -z "$*" ]
then
	command='/bin/ash'
else
	command="wp $@"
fi

docker run -it --rm --network $NETWORK --volumes-from $WP_CONTAINER wordpress:cli $command
