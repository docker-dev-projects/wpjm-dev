<?php

// Set up debugging.
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', false );

// Turn on REST API.
define( 'WPJM_REST_API_ENABLED', true );
